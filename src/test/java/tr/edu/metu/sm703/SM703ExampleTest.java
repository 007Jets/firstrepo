package tr.edu.metu.sm703;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class SM703ExampleTest {

    private SM703Example sm703CourseProject = new SM703Example();

    @Test
    public void CheckAddForPositives(){ assertEquals(sm703CourseProject.addIntegers(1, 2), 3); }

    @Test
    public void CheckAddForZero(){
        assertEquals(sm703CourseProject.addIntegers(0, 2), 2);
    }

    @Test
    public void CheckAddForNegatives(){
        assertEquals(sm703CourseProject.addIntegers(-1, -2), -3);
    }

    @Test
    public void CheckAddThreeForPositives(){
        assertEquals(sm703CourseProject.addThreeIntegers(1, 2, 3), 6);
    }

    @Test
    public void CheckAddThreeForZero(){
        assertEquals(sm703CourseProject.addThreeIntegers(0, 0, 2), 2);
    }

    @Test
    public void CheckAddThreeForNegatives(){
        assertEquals(sm703CourseProject.addThreeIntegers(-1, -2, -3), -6);
    }
}
