package tr.edu.metu.sm703;

import java.util.Map;

import static java.util.Objects.isNull;

public class SM703Example {

    public int addIntegers(int o1, int o2){
        return o1 + o2;
    }

    public int addThreeIntegers(int p1, int p2, int p3){
        return p1 + p2 + p3;
    }

}
